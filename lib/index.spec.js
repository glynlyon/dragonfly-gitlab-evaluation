/**
 * index.spec
 */
const request = require('supertest');
const { expect } = require("chai");
const { app, server } = require('./index');

describe('Index', () => {

    after(() => {
        server.close();
    });

    it('GET / should return the correct message', (done) => {
        request(app)
            .get('/')
            .expect(200)
            .end((err, res) => {
                expect(res.body.message).to.equal('I am here!');
                done();
            });
    });

    it('GET /?location={location} should return the correct message', (done) => {
        request(app)
            .get('/')
            .query({ location: 'home' })
            .expect(200)
            .end((err, res) => {
                expect(res.body.message).to.equal('I am at home');
                done();
            });
    });
});