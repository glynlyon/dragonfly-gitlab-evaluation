/**
 * Simple POC web service
 */
const express = require('express');
const app = express();
const router = express.Router();

/**
 * @async GET /
 * @param {HttpRequest} req
 * @param {HttpResponse} res
 */
router.get('/', (req, res) => {

    let message = 'I am here!';

    if(req.query.location) {
        message = 'I am at ' + req.query.location;
    }

    res.json({
        message: message
    });
    return;
});

app.use('', router);

const server = app.listen(process.env.PORT || 4500);

module.exports = { app, server, router };